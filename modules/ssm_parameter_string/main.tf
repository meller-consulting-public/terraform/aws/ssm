variable "name" {
  type        = string
  description = "Name of the SSM parameter"
  default     = ""
}

variable "value" {
  type        = string
  description = "Value of the SSM parameter"
  default     = ""
}

variable "description" {
  type        = string
  description = "Description of the SSM parameter"
  default     = ""
}

resource "aws_ssm_parameter" "unencrypted_parameter" {
  name        = var.name
  description = var.description
  type        = "String"
  value       = var.value

  validation {
    condition     = length(var.name) > 0
    error_message = "Name must have a length greater than zero."
  }

  validation {
    condition     = length(var.value) > 0
    error_message = "Value must have a length greater than zero."
  }

  validation {
    condition     = length(var.description) > 0
    error_message = "Description must have a length greater than zero."
  }

  tags = {
    type = "unencrypted"
  }
}