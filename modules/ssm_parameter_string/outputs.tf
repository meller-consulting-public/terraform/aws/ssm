output "arn" {
  value       = aws_ssm_parameter.unencrypted_parameter.arn
  description = "ARN of the SSM parameter"
}

output "_id" {
  value       = aws_ssm_parameter.unencrypted_parameter.id
  description = "ID of the SSM parameter"
}

output "key_name" {
  value       = aws_ssm_parameter.unencrypted_parameter.key_name
  description = "Key name of the SSM parameter"
}