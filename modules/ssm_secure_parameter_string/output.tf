output "arn" {
  value       = aws_ssm_parameter.encrypted_parameter.arn
  description = "ARN of the SSM parameter"
}

output "id" {
  value       = aws_ssm_parameter.encrypted_parameter.id
  description = "ID of the SSM parameter"
}

output "key_name" {
  value       = aws_ssm_parameter.encrypted_parameter.key_name
  description = "Key name of the SSM parameter"
}