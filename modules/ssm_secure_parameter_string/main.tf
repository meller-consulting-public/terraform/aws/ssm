variable "name" {
  type        = string
  description = "Name of the SSM parameter"
  default     = ""
}

variable "value" {
  type        = string
  description = "Value of the SSM parameter"
  default     = ""
}

variable "description" {
  type        = string
  description = "Description of the SSM parameter"
  default     = ""
}

variable "key_alias" {
  description = "Alias of the KMS key used for encryption"
  type        = string
}

resource "aws_ssm_parameter" "encrypted_parameter" {
  name        = var.name
  description = var.description
  type        = "String"
  value       = var.value
  key_id      = aws_kms_key.parameter_store.arn

  validation {
    condition     = length(var.name) > 0
    error_message = "Name must have a length greater than zero."
  }

  validation {
    condition     = length(var.value) > 0
    error_message = "Value must have a length greater than zero."
  }

  validation {
    condition     = length(var.description) > 0
    error_message = "Description must have a length greater than zero."
  }

  tags = {
    type = "encrypted"
  }
}

resource "aws_kms_alias" "parameter_store" {
  name          = var.key_alias
  target_key_id = aws_kms_key.parameter_store.key_id
}
