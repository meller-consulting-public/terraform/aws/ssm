# terraform-gitlab-projects
![IMAGE_DESCRIPTION](https://gitlab.com/meller-consulting-public/terraform/aws/ssm/badges/main/pipeline.svg?ignore_skipped=true)

## Overview
This project contains Terraform modules for the creation of AWS SSM Parameters.

Currently there are two types supported:
* Encrypted strings
* Unencrypted strings

## Modules
### Encrypted Strings

| Input       | Description                      | Default | Validation |
|:------------|:---------------------------------|:--------|:-----------|
| name        | Name of the SSM parameter        |_blank_  | Not blank  |
| description | Description of the SSM parameter |_blank_  | Not blank  |
| value       | value of the SSM parameter       |_blank_  | Not blank  |

### Unencrypted Strings

| Input       | Description                              | Default | Validation |
|:------------|:-----------------------------------------|:--------|:-----------|
| name        | Name of the SSM parameter                | _blank_ | Not blank  |
| description | Description of the SSM parameter         | _blank_ | Not blank  |
| value       | value of the SSM parameter               | _blank_ | Not blank  |
| key_alias   | Alias of the KMS key used for encryption |    _required_     |            |
